import csv

#open slcp.csv
with open('slcsp.csv', 'r') as slcp_csv_file:
    slcp_csv_reader = csv.reader(slcp_csv_file)
    next(slcp_csv_reader)
    slcsp_data = list(slcp_csv_reader)
#open zips.csv
with open('zips.csv', 'r') as zips_csv_file:
    zips_csv_reader = csv.reader(zips_csv_file)
    next(zips_csv_reader)
    zips_data = list(zips_csv_reader)
#open plans.csv
with open('plans.csv', 'r') as plans_csv_file:
    plans_csv_reader = csv.reader(plans_csv_file)
    next(plans_csv_reader)
    plans_data = list(plans_csv_reader)

#collects zipcodes in slcsp.csv
    slcsp_zips = []
    slcsp_zip_col = 0
    for slcsp_zip in range(len(slcsp_data)):
        slcsp_zips.append(slcsp_data[slcsp_zip_col][0])
        slcsp_zip_col += 1
    #print(slcsp_zips)

#collects zipcodes with corresponding state and rate_area in zips.csv in a dictionary
    zips_zips = []
    zips_zip_col = 0
    for zip_zip in range(len(zips_data)):
        zips_zips.append(zips_data[zips_zip_col][0])
        zips_zip_col += 1
    #print(zips_zips)

#compare slcsp zips with zips.cs zips and collect correspoding stae and rate_area
    zips_state = []
    slcsp_zips_col = 0
    zips_zips_col = 0

    #for x in range(1, 50):
    while slcsp_zips_col <= len(slcsp_zips):
        for xx in range(len(zips_zips)):
            while slcsp_zips[slcsp_zips_col] != zips_zips[zips_zips_col]:
                zips_zips_col += 1
            if slcsp_zips[slcsp_zips_col] == zips_zips[zips_zips_col]:
                zips_state.append(zips_data[zips_zips_col][1])
                slcsp_zips_col += 1
                zips_zips_col = 0
    print(zips_state)
